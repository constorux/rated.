package org.robin.rated;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;

import java.io.IOException;

public class SoundPlayer {

    Context _context;
    final static MediaPlayer _mp = new MediaPlayer();


    public SoundPlayer(Context context)
    {
        _context = context;
    }


    public void playSound(int rating)
    {
        Vibrator v = (Vibrator) _context.getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(50);
        }


        if (_mp.isPlaying()) {
            _mp.stop();
        }

        try {
            _mp.reset();
            AssetFileDescriptor afd = null;

            if((rating >= 1)&&(rating <= 5))
            {
                afd = _context.getAssets().openFd("rate_" + rating + ".mp3");
            }
            else
            {
                //TODO
                return;
            }

            _mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            _mp.prepare();
            _mp.start();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
