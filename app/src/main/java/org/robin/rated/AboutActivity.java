package org.robin.rated;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        overridePendingTransition( 0, R.anim.fade_out_animation );


        TextView txtLicense = findViewById(R.id.txtLicense);

        InputStream inputStream = getResources().openRawResource(R.raw.license);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int i;
        try {
            i = inputStream.read();
            while (i != -1)
            {
                byteArrayOutputStream.write(i);
                i = inputStream.read();
            }
            inputStream.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        txtLicense.setText(byteArrayOutputStream.toString());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition( 0, R.anim.fade_out_animation );
    }

    @Override public void finish()
    {
        overridePendingTransition( 0, R.anim.fade_out_animation );
        super.finish();
    }
}
