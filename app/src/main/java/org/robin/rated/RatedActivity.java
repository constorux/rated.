package org.robin.rated;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class RatedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rated);

        final Window win= getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        TextView txtRating = findViewById(R.id.txtYourAVG);
        TextView txtRater = findViewById(R.id.txtRater);
        RatingBar ratRating = findViewById(R.id.ratOtherRating);
        ImageView imgBackgroud = findViewById(R.id.imgBackgroundRated);

        FileManager _fileMgr = new FileManager(RatedActivity.this);
        txtRating.setText(_fileMgr.getAVG() + "");

        ratRating.setRating(getIntent().getIntExtra("INCOMING_RATING", 0));

        txtRater.setText(Html.fromHtml("by a <b> " + getIntent().getDoubleExtra("INCOMING_RATER", 5.0) + "</b> member"));

        new SoundPlayer(RatedActivity.this).playSound(0);

        imgBackgroud.setOnTouchListener(new OnSwipeTouchListener(RatedActivity.this) {
            public void onSwipeTop() {
                exitActivity();
            }

            public void onSwipeLeft() {
                exitActivity();
            }

            public void onSwipeRight() {
                exitActivity();
            }

            public void onSwipeBottom() {
                exitActivity();
            }
        });


    }


    private void exitActivity() {
        finish();
        overridePendingTransition( 0, R.anim.fade_out_animation );
    }
}
