package org.robin.rated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStreamWriter;

public class SmsListener extends BroadcastReceiver {

    private SharedPreferences preferences;

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub

        if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
            Bundle bundle = intent.getExtras();           //---get the SMS message passed in---
            SmsMessage[] msgs = null;
            String msg_from;
            if (bundle != null){
                //---retrieve the SMS message received---
                try{
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    msgs = new SmsMessage[pdus.length];
                    for(int i=0; i<msgs.length; i++){
                        msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                        msg_from = msgs[i].getOriginatingAddress();
                        String msgBody = msgs[i].getMessageBody();

                        if(msgBody.startsWith("You have been rated "))
                        {
                            FileManager fileMgr = new FileManager(context);
                            int sentRating = 0;
                            if(msgBody.contains("1 "))
                            {
                                sentRating = 1;
                            }
                            if(msgBody.contains("2 "))
                            {
                                sentRating = 2;
                            }
                            if(msgBody.contains("3 "))
                            {
                                sentRating = 3;
                            }
                            if(msgBody.contains("4 "))
                            {
                                sentRating = 4;
                            }
                            if(msgBody.contains("5 "))
                            {
                                sentRating = 5;
                            }

                            fileMgr.storeAVG(sentRating);

                            double senderRating = 5.0;
                            senderRating = Double.parseDouble(msgBody.substring(msgBody.lastIndexOf(' ') + 1));

                            Intent PopupIntend = new Intent(context, RatedActivity.class);
                            PopupIntend.putExtra("INCOMING_RATING", sentRating);
                            PopupIntend.putExtra("INCOMING_RATER", senderRating);

                            context.startActivity(PopupIntend);
                        }
                    }
                }catch(Exception e){
//                            Log.d("Exception caught",e.getMessage());
                }
            }
        }
    }

    private void writeToFile(String data,Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("config.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
}