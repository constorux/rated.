package org.robin.rated;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class FileManager {

    String _fileName = "config.txt";
    Context _context;


    public FileManager(Context context)
    {
        _context = context;
    }

    public void storeAVG(int newValue)
    {
        Double[] oldValues = getAVGdata();

        Double avg = (((oldValues[0])*oldValues[1]) + newValue)/(oldValues[1] + 1);

        writeToFile(avg.toString() + ";" + oldValues[1] + 1);

    }


    public double getAVG()
    {
        return (getAVGdata()[0]);
    }


    public Double[] getAVGdata()
    {
        Double[] avg = new Double[2];
        avg[0] = 5.0;
        avg[1] = 0.0;

        String[] parts = readFromFile().split(";");
        if(parts.length >= 2)
        {
            avg[0] = Double.parseDouble(parts[0]);
            avg[0] = (double) Math.round((avg[0] * 100));
            avg[0] = avg[0]/100;
            avg[1] = Double.parseDouble(parts[1]);
        }

        return avg;
    }

    private void writeToFile(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(_context.openFileOutput(_fileName, Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    private String readFromFile() {

        String ret = "";

        try {
            InputStream inputStream = _context.openFileInput(_fileName);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }



}