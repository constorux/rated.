package org.robin.rated;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    final MediaPlayer _mp = new MediaPlayer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imgBackgroud = findViewById(R.id.imgBackgroundRated);
        TextView txtRating = findViewById(R.id.txtYourRating);
        Button buttonAbout = findViewById(R.id.btnAbout);

        final EditText txtphoneNr = findViewById(R.id.txtPhoneNumber);
        final RatingBar blackMirrorBar = (RatingBar) findViewById(R.id.blackMirrorBar);

        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {

            Toast.makeText(getApplicationContext(), "if you want to let the app send your ratings please grant SMS permission",
                    Toast.LENGTH_LONG).show();

            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.SEND_SMS},
                    123);

        }

        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.RECEIVE_SMS)
                != PackageManager.PERMISSION_GRANTED) {

            Toast.makeText(getApplicationContext(), "if you want to let the app send your ratings please grant SMS permission",
                    Toast.LENGTH_LONG).show();

            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.RECEIVE_SMS},
                    124);

        }





        imgBackgroud.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this) {
            public void onSwipeTop() {
                if(((int) blackMirrorBar.getRating()) >= 1) {
                    new SoundPlayer(MainActivity.this).playSound((int) blackMirrorBar.getRating());
                    if ((txtphoneNr.getText() != null) && (!txtphoneNr.getText().toString().equals(""))) {
                        sendMessage(txtphoneNr.getText().toString(), "You have been rated " + ((int) blackMirrorBar.getRating()) + " stars by a member with an average rating of " + new FileManager(MainActivity.this).getAVG());
                    }
                }
            }

            public void onSwipeBottom() {
                blackMirrorBar.setRating(0);
            }
        });


        buttonAbout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, AboutActivity.class);
                MainActivity.this.startActivity(myIntent);// Code here executes on main thread after user presses button
            }
        });


        FileManager _fileMgr = new FileManager(MainActivity.this);
        txtRating.setText(_fileMgr.getAVG() + "");

    }




    void sendMessage(String phoneNumber, String message)
    {
        /*Intent intent=new Intent(getApplicationContext(),MainActivity.class);
        PendingIntent pi=PendingIntent.getActivity(getApplicationContext(), 0, intent,0);

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, pi,null);*/
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {

            Toast.makeText(getApplicationContext(), "you need to grant SMS permission for this feature",
                    Toast.LENGTH_LONG).show();

            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.SEND_SMS},
                    123);

        }
        else {
            try {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(phoneNumber, null, message, null, null);
                Toast.makeText(getApplicationContext(), "rating sent",
                        Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(),
                        "SMS faild, please try again later!",
                        Toast.LENGTH_LONG).show();
                e.printStackTrace();
                e.printStackTrace();
            }
        }

    }


}
