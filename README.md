# rated.

This is my implementation of an App/social-network which appeared in the 3rd season of the TV series 'Black Mirror'. I started this project as a little fun project for myself, which is why I did not test all the features on other devices yet. Nonetheless you are most welcome to try the app for yourself.

## Download

To use this app you can just install the APK file, which you can find above. It also requires SMS permission to work fully. This enables you to send ratings to your friends.

## contribute

If you find a bug, want to contribute to the project, or suggest a feature feel free to contact me. I am not currently working on the project
